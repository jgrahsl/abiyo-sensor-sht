#!/usr/bin/python3

import paho.mqtt.client as mqtt
import bluepy
from bluepy.btle import UUID, Peripheral, DefaultDelegate, AssignedNumbers
import math
import struct
import time
import json
import sys
import requests
import os

INFLUX_USER=os.environ.get('INFLUX_USER', default='/tmp')
INFLUX_PASS=os.environ.get('INFLUX_PASS', default='/tmp')
INFLUX_HOST=os.environ.get('INFLUX_HOST', default='/tmp')
INFLUX_DB=os.environ.get('INFLUX_DB', default='/tmp')


class Monitor:

    def __init__(self, bt, key):
        self.bt = bt
        self.key = key
        pass
            
    def setup(self):
        try:
            print("connecting")
            self.p = Peripheral(self.bt, bluepy.btle.ADDR_TYPE_RANDOM)
            print("connecteed")
            self.humid = self.p.getCharacteristics(uuid=UUID("00001235-b38d-4985-720e-0f993a68ee41"))
            self.temp = self.p.getCharacteristics(uuid=UUID("00002235-b38d-4985-720e-0F993a68ee41"))
            self.bat = self.p.getCharacteristics(uuid=UUID("2A19"))
        except Exception as e:
            print(str(e))
        else:
            print("Conn ok")
            
    def measure(self):
        m = {}
        m["humidity"] = round(struct.unpack("<f", self.humid[0].read())[0],2)
        m["temperature"] = round(struct.unpack("<f", self.temp[0].read())[0],2)
        m["battery_level"] = struct.unpack("b", self.bat[0].read())[0]

        t = m["temperature"]
        rh = m["humidity"]

        m["humidity_absolute"] = round( (6.112*math.exp((17.67*t)/(t+243.5))*rh*2.1674)/(273.15+t),2)

        return m


        
mon = Monitor('F3:C6:52:21:28:1A', 0) # os.environ['MONITOR_KEY'])
mon.setup()

cfgMqttServer = "192.168.0.185"
cfgMqttUser = "" # leave empty if no user/password is needed
cfgMqttPassword = "" # leave empty if no user/password is needed

client = mqtt.Client("nm")

client.connect(cfgMqttServer)
client.loop_start()

while True:
    try:
        m = mon.measure()
        for key, value in m.items():
            client.publish("abiyo/sensor/zone1/" + key, value)

    except Exception as e:
        print(e)
        break

    print(json.dumps(m))
    sys.stdout.flush()
    time.sleep(5)


